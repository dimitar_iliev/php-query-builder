<?php
namespace Models;

use \QueryBuilder\QueryBuilder;

class Model  implements \JsonSerializable
{

    protected $hidden = [];
    protected $_fields = [];
    protected $hydrated = false;
    protected $modelId = 'Id';

    static public $provider = 'glob';

    public function jsonSerialize() {
        return $this->_fields;
    }

    static public function __callStatic($name, $arguments)
    {
        $queryBuilderClass = QueryBuilder::class;

        if(in_array($name, get_class_methods($queryBuilderClass)))
        {
            $queryBuilder = new QueryBuilder(get_called_class());
            return call_user_func_array([$queryBuilder, $name], $arguments);
        }
    }

    public function __call($name, $arguments)
    {
        $queryBuilderClass = QueryBuilder::class;
        $modelId = $this->modelId;

        if(in_array($name, get_class_methods($queryBuilderClass)))
        {
            $queryBuilder = new QueryBuilder(get_called_class());
            if($this->hydrated && $this->$modelId) {

                $queryBuilder->where($modelId, $this->$modelId);
            }

            return call_user_func_array([$queryBuilder, $name], $arguments);
        }
    }

    public function __get($name)
    {
        if(isset($this->_fields[$name])) return $this->_fields[$name];
        throw new \Exception("Model does not have property $name");
    }

    public function hydrate($data)
    {
        foreach($data as $key => $val)
        {
            $this->setField($key, $val);
        }

        $this->hydrated = true;

        return $this;
    }

    public function setField($name, $value)
    {
        if($this->isPropertyHidden($name) == false) 
        {
            $this->_fields[$name] = $value;
        }
    }

    protected function isPropertyHidden($key)
    {
        return in_array($key, $this->hidden);
    }

    static public function deleting($input)
    {
        return $input;
    }

    static public function creating($input)
    {
        return $input;
    }

    static public function updating($input)
    {
        return $input;
    }

    static public function getProvider()
    {
        return static::$provider;
    }

    static public function getTable()
    {
        return static::$tableName;
    }
}