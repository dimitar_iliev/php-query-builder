<?php
namespace QueryBuilder;

class ColumnsCollection {
    protected $columns;

    public function __constuct($columns = []) {
        $this->columns = [];
        foreach($columns as $col) {
            $this->columns[] = new DbColumn($col);
        }
    }

    public function add($column) {
        $this->columns[] = new DbColumn($column);
        return $this;
    }

    public function __toString() {
        return implode(', ', $this->columns);
    }
}