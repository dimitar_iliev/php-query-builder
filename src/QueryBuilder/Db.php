<?php
namespace QueryBuilder;

class Db
{
    static public function Now()
    {
        return new Raw('NOW()');
    }

    static public function Null()
    {
        return new Raw('NULL');
    }
}