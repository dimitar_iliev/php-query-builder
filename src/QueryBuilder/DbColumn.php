<?php
namespace QueryBuilder;

class DbColumn {
    protected $columnName;
    protected $tableName;

    // Prevent double column escape
    protected $cloneInstance;

    public function __construct($columnName, $tableName = null) {
        if($columnName instanceof DbColumn) {
            $this->cloneInstane = $columnName;   
        } else {
            $this->columnName = $columnName;
            $this->tableName = $tableName;
        }
    }

    protected function wrap($val) {
        $val = implode('`.`', explode(".", $val));
        return "`$val`";
    }

    public function __toString() {
        if(!empty($this->cloneInstance)) return $this->cloneInstance."";
        if(empty($this->tableName)) return $this->wrap($this->columnName);

        return implode(".", [
            $this->wrap($this->tableName),
            $this->wrap($this->columnName)
        ]);
    }
}