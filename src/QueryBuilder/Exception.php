<?php

namespace QueryBuilder;

class Exception extends \Exception
{
    static public function invalidOrWhere()
    {
        throw new Exception("Invalid OrWhere.");
    }
}