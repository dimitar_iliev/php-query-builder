<?php
namespace QueryBuilder;

class QueryBuilder extends QueryBuilderCore
{
    protected $where;
    protected $limit;
    protected $table;
    protected $alias;
    protected $_queryString = '';
    protected $joins;

    protected $orderBy = '';

    protected $classInstance;
    protected $orFlag = false; // used to switch on/on 'OR' statement

    public function __construct($hydrateClass, $options = [])
    {
        $this->joins = [];
        $this->table = $hydrateClass::getTable();//$tableName;
        $alias = $hydrateClass::getAlias();
        
        if($alias == $this->table) $this->alias = '';
        else $this->alias = $alias;

        $this->hydrateClass = $hydrateClass;

        $this->where = $options['where'] ?? [];
        $this->joins = $options['join'] ?? [];
        $this->limit = $options['limit'] ?? '';
        $this->orderBy = $options['orderBy'] ?? '';
        $this->orFlag = $options['orFlag'] ?? false;
    }

    public function orderBy($orderBy)
    {
        return $this->orderByAsc($orderBy);
    }

    public function orderByAsc($orderBy)
    {
        $this->orderBy = $orderBy.' asc';
        return $this;
    }

    public function orderByDesc($orderBy)
    {
        $this->orderBy = $orderBy.' desc';
        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function orWhere($field, $operator = null, $value = null)
    {
        /**
         * Consider this case
         * Domain::where('valid', 1)->orWhere('valid', 2)->orWhere('valid', 3)
         * 
         * should it produce something like SELECT * FROM $table WHERE (valid = 1 OR valid = 2 OR valid = 3)
         */

        if(count($this->where) == 0) return $this->where($field, $operator, $value);
        $where = array_pop($this->where);

        if(is_null($value)) {
            $value = $operator;
            $operator = '=';
        }

        if($this->isRaw($field)) {
            $orWhere = $field;
        } else {
            $value = $this->prepareValue($value);
            $orWhere = implode(' ', [ Raw::column($field), $operator, $value]);
        }

        if(is_array($field)) {

        }

        
        if(is_array($where)) {
            $where[] = $orWhere;
            $expression = $where;
        } else {
            $expression = [$where, $orWhere]; // $this->wrapExpression(implode(' OR ', ));
        }

        $this->where[] = $expression; //Db::raw($expression);

        return $this;
    }

    public function where($field, $operator = null, $value = null)
    {
        if(is_array($field)) {
            foreach($field as $key => $value) {
                $this->where($key, $value);
            }

            return $this;
        }

        if($this->isRaw($field)) {
            $this->where[] = $field;
            return $this;
        }

        if(is_null($value)) {
            $value = $operator;
            $operator = '=';
        }

        $value = $this->prepareValue($value);

        $strCondition = implode(' ', [ new DbColumn($field), $operator, $value]);
        if($this->orFlag) {
            $lastCondition = array_pop($this->where) ?? [];
            
            if(is_array($lastCondition)) $lastCondition[] = $strCondition;
            else $lastCondition = [$lastCondition, $strCondition];
            $this->where[] = $lastCondition;
        } else {
            $this->where[] = $strCondition;
        }

        return $this;
    }

    public function whereIn($field, $value = null)
    {
        $operator = 'IN';
        if(is_array($value) && count($value) == 0) return $this;

        if(is_array($field)) {
            foreach($field as $key => $val) {
                $this->whereIn($key, $val);
            }

            return $this;
        }
        
        $value = $this->prepareValue($value);
        $strCondition = implode(' ', [ new DbColumn($field), $operator, '(', $value, ')']);

        if($this->orFlag) {
            $lastCondition = array_pop($this->where);
            
            if(is_array($lastCondition)) $lastCondition[] = $strCondition;
            else $lastCondition = [$lastCondition, $strCondition];
            $this->where[] = $lastCondition;
        } else {
            $this->where[] = $strCondition;
        }

        return $this;
    }

    public function whereNotIn($field, $value = null)
    {
        $operator = 'NOT IN';
        if(is_array($value) && count($value) == 0) return $this;

        if(is_array($field)) {
            foreach($field as $key => $val) {
                $this->whereNotIn($key, $val);
            }

            return $this;
        }
        
        $value = $this->prepareValue($value);
        $strCondition = implode(' ', [ new DbColumn($field), $operator, '(', $value, ')']);

        if($this->orFlag) {
            $lastCondition = array_pop($this->where);
            
            if(is_array($lastCondition)) $lastCondition[] = $strCondition;
            else $lastCondition = [$lastCondition, $strCondition];
            $this->where[] = $lastCondition;
        } else {
            $this->where[] = $strCondition;
        }

        return $this;
    }

    public function find($id, $fields = '*')
    {
        return $this->where('Id', (int) $id)->first($fields);
    }

    public function first($fields = '*')
    {
        $this->limit = '1';
        $results = $this->get($fields);
        
        if(count($results) > 0) return $results[0];
        return null;
    }

    public function setOr()
    {
        $this->orFlag = true;
        return $this;
    }

    public function unsetOr()
    {
        $this->orFlag = false;
        return $this;
    }

    protected function hydrate($data)
    {
        $modelClass = $this->hydrateClass;
        $model = new $modelClass();
        $model->hydrate($data);

        return $model;
    }

    protected function buildSelect()
    {      
        $statements = [
            implode(' ', array_filter(['select', $this->fields, 'from', $this->table, $this->alias])),
            $this->joinStatement(),
            $this->whereStatement(),
            $this->orderbyStatement(),
            $this->limitStatement()
        ];

        $this->_queryString = implode(' ', array_filter($statements));
    }

    protected function buildCreate()
    {
        $CLASS = $this->hydrateClass;
        $createFields = $CLASS::creating($this->fields);
        $columnsCollection = new ColumnsCollection();

        foreach($createFields as $field => $val)
        {   
            // $keys[] = $field;
            $columnsCollection->add($field);
            $values[] = $this->prepareValue($val);
        }

        // $tableColumns = $columnsCollection; // implode(', ', $this->wrapColumns($keys));
        $tableValues = implode(', ', $values);

        $statements = [
            'insert into',
            $this->table,
            $this->wrapExpression($columnsCollection),
            'values',
            $this->wrapExpression($tableValues)
        ];

        $this->_queryString = implode(' ', $statements);
    }

    protected function buildUpdate()
    {
        $fields = [];
        $CLASS = $this->hydrateClass;
        $updateFields = $CLASS::updating($this->fields);

        foreach($updateFields as $field => $val)
        {
            $fields[] = new DbColumn($field).' = '.$this->prepareValue($val);
        }

        $statements = [
            implode(' ', ['update', $this->table, 'set', implode(', ', $fields)]),
            $this->whereStatement()
        ];

        $this->_queryString = implode(' ', array_filter($statements));
    }

    protected function buildDelete()
    {
        $CLASS = $this->hydrateClass;
        $CLASS::deleting($this->where);
       
        $statements = [
            implode(' ', ['delete from', $this->table]),
            $this->whereStatement()
        ];

        $this->_queryString = implode(' ', array_filter($statements));
    }


    protected function whereStatement()
    {   
        $whereStatements = array_filter($this->where ?? []); // filter empty where statements

        if(empty($whereStatements)) return '';

        $whereStatements = $this->reduceWhereStatements($whereStatements);


        return 'where '.implode(' and ', $whereStatements). ' ';
    }

    protected function joinStatement()
    {  

        return implode(" ", $this->joins);
    }

    protected function reduceWhereStatements($statements) {
        $result = [];
        foreach($statements as $statement) {
            if(is_array($statement)) {
                $result[] = $this->wrapExpression(
                    implode(' or ', $this->reduceWhereStatements($statement))
                );
            }
            else $result[] = $statement;
        }
        
        return $result;
    }

    protected function limitStatement()
    {
        if(empty($this->limit)) return '';
        if(is_array($this->limit)) return 'limit '.implode(',', $this->limit);
        
        return 'limit '.$this->limit;
    }

    protected function orderbyStatement()
    {
        if(empty($this->orderBy)) return '';

        return 'order by '.$this->orderBy;
    }


    public function get($fields = '*', $hydrate = true)
    {
        $out = [];
        $this->fields = $fields;
        $this->buildSelect();
        $qry = $this->query($this->_queryString);

        while($row = $qry->fetch_assoc()) {
            $out[] = $hydrate ? $this->hydrate($row) : $row;
        }

        return $out;
    }

    public function column($field) {
        $out = [];
        $this->fields = $field;
        $this->buildSelect();
        $qry = $this->query($this->_queryString);

        $split = explode(' as ', $field);
        $_field = $split[1] ?? $field;
        
        while($row = $qry->fetch_assoc()) {
            $out[] = $row[$_field];
        }

        return $out;
    }

    public function count($field) {
        $this->debugOnPost = 1;
        $column = Raw::column($field);
        $this->fields = "count($column) as `count`";
        $this->buildSelect();

        $qry = $this->query($this->_queryString);
        $out = 0;
        while($row = $qry->fetch_assoc()) {
            $out = $row["count"];
        }

        return (int) $out;
    }

    public function random($fields, $needed, $countKey = 'Id', $hydrate = true) {
        $total = $this->count($countKey);
        $offset = rand(0, max(0, $total - $needed));
        $this->limit("$offset, $needed");

        return $this->get($fields, $hydrate);
    }

    public function randomColumn($field, $needed, $countKey = 'Id') {
        $total = $this->count($countKey);
        $offset = rand(0, max(0, $total - $needed));
        $this->limit("$offset, $needed");

        return $this->column($field);
    }


    public function update($fields)
    {
        $this->fields = $fields;
        $this->buildUpdate();
        $qry = $this->query($this->_queryString);// \db::query($this->_queryString, 'glob');
    }

    public function create($fields)
    {
        $this->fields = $fields;
        $this->buildCreate();

        $qry = $this->query($this->_queryString);
        return $this;
    }

    public function createMany($items)
    {
        foreach($items as $item) {
            $this->create($item);
        }
    }

    public function join($class, $expression, $alias = null) {
        $alias = $alias ?? $class::getAlias();
        $this->joins[] = "JOIN " . (new Raw($class::getTable(). " $alias ON ". $expression));
        return $this;
    }

    public function leftJoin($class, $expression, $alias = null) {
        $alias = $alias ?? $class::getAlias();
        $this->joins[] = "LEFT JOIN " . (new Raw($class::getTable(). " $alias ON ". $expression));
        return $this;
    }

    public function delete()
    {
        $this->buildDelete();
        $qry = $this->query($this->_queryString);
    }

    public function insertId() {
        // return 42;
        $res = \db::cid($this->getProvider());
        return $res->insert_id;
    }

    public function clone() {
        return new QueryBuilder($this->hydrateClass, [
            'where' => $this->where,
            'limit' => $this->limit,
            'joins' => $this->joins,
            'orderBy' => $this->orderBy,
            'orFlag' => $this->orFlag
        ]);
    }

}