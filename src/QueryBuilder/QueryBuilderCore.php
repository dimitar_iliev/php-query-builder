<?php
namespace QueryBuilder;

use \Exception;

class QueryBuilderCore
{
    public $debug = 0;
    public $debugOnPost = 0;

    protected $hydrateClass;
    protected $fields = '';

    protected $logQueries = 0;
    protected $logFile = '/var/www/logs/dbqueries';

    protected function wrapString($val)
    {
        return "'$val'";
    }
   
    protected function wrapExpression($value)
    {
        return "( $value )";
    }

    protected function wrapColumns($columns)
    {   $cols = [];
        foreach($columns as $col) {
            $cols[] = "`${col}`"; // => `Id`, `key`, `value`,...
        }
        return $cols;
    }
    
    protected function query($string)
    {
        $provider = $this->getProvider();

        if($this->debug || ($this->debugOnPost && !empty($_POST))) {
            echo "QueryBuilder debug is on <!--";
            var_dump($string);
            echo "-->";
        }

        if($this->logQueries) {
            $logfile = $this->logFile."-".date("Ymd").'.log';
            $f = fopen($logfile, "a+");

            fwrite($f,
                "[ ".date('H.i:s')." ".$_SERVER['REQUEST_METHOD']. " ".$_SERVER['SERVER_PROTOCOL']." ". $_SERVER['REQUEST_URI']." ] SQL::{{".$string."}} provider '".$provider."'\n\r"
            );
            fclose($f);
        }

        return \db::query($string, $provider);
    }

    protected function escape($data)
    {
        $provider = $this->getProvider();
        return \db::e($data, $provider);
    }

    protected function escapeString($data)
    {
        return $this->wrapString($this->escape($data));
    }
    
    protected function isRaw($value)
    {
        return $value instanceof Raw;
    }
 
    protected function prepareValue($val)
    {
        if(is_null($val)) {
            return Db::Null()->value();
        }

        if($this->isRaw($val)) {
            return $val->value();
        } 

        if(is_numeric($val)) {
            return $this->escape($val);
        }
        
        if(is_string($val)) {
            return $this->escapeString($val);
        }

        if(is_callable($val)) {
            return $this->prepareValue($val());
        }

        if(empty($val)) {
            return $this->escapeString('');
        }

        if(is_array($val)) {
            return implode(',', array_map([$this, 'prepareValue'], $val));
        }

        throw new Exception("Cannot prepare sql value");
    }

    protected function getProvider()
    {
        $CLASS = $this->hydrateClass;
        return $CLASS::getProvider();
    }
}