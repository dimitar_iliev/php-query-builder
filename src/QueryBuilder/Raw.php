<?php
namespace QueryBuilder;

class Raw
{
    protected $val;
    public function __construct($val)
    {
        $this->val = $val;
    }

    public function value()
    {
        return $this->val;
    }

    public static function raw($value)
    {
        return new Raw($value);
    }

    public static function column($field) {
        return new DbColumn($field);
    }

    public function __toString() {
        return $this->val;
    }
}