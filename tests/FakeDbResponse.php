<?php
namespace Test;

class FakeDbResponse
{
    protected $data;
    protected $fetched = false;

    public function __construct($data = null)
    {
        $this->data = $data;
    }


    public function fetch_assoc()
    {
        if($this->fetched) return null;
        $this->fetched = true;
        return $this->data;
    }
}