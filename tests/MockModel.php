<?php
namespace Test;

use \Models\Model;

class MockModel extends Model
{
    public static $tableName = 'mocktable';
    protected $hidden = ['hiddenProperty'];
}