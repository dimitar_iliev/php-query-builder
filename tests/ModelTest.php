<?php
namespace Test;

use PHPUnit\Framework\TestCase;
use \QueryBuilder\QueryBuilder;

final class ModelTest extends TestCase
{
    public function assertInstanceOfQueryBuilder($instance)
    {
        return $this->assertInstanceOf(QueryBuilder::class, $instance);
    }

    public function getUnmocked()
    {
        return new MockModel();
    }

    public function getMock()
    {
        $mock = $this->getMockBuilder(MockModel::class)
                     ->getMock();
        
        return $mock;
    }

    public function testModelCanSetField() : void
    {
        $model = $this->getUnmocked();
        
        $value = 'bar';
        $field = 'foo';

        $model->setField($field, $value);

        $this->assertEquals($model->$field, $value);
    }

    public function testModelDoesNotSetHiddenProperties() : void
    {
        $model = $this->getUnmocked();
        $value = 'bar';
        $field = 'hiddenProperty'; // already set in MockModel

        $model->setField($field, $value);
        $expectionThrown = false;

        try {
            $this->assertEquals($model->$field, 'Some totaly invalid value which should never be equal to '.$value);
        } catch(\Exception $exp)
        {
            $expectionThrown = true;
            $this->assertEquals($exp->getMessage(), 'Model does not have property hiddenProperty');
        }

        $this->assertTrue($expectionThrown);
        
    }

    // public function testModelReturnsQueryBuilder() : void
    // {
    //     // $this->markTestIncomplete(
    //     //     'Skip Test. Unable to mock QueryBuilder::escapeString with connects to DB. Those methods should be tested in QueryBuilder Test instead.'
    //     // );

    //     $model = $this->getUnmocked();

    //     $limitQb = $model->limit(1);
    //     $orderQb = $model->orderBy('field');
    //     $whereQb1 = $model->where('field', 'value');

    //     $whereQb2 = $model->where([
    //         'field' => 'value',
    //         'field2' => 'value2'
    //     ]);

    //     $whereQb3 = $model->where('field', '!=', 'value');

    //     foreach([$limitQb, $orderQb, $whereQb, $whereQb1, $whereQb2] as $qb)
    //     {
    //         $this->assertInstanceOfQueryBuilder($qb);
    //     }
    // }
}