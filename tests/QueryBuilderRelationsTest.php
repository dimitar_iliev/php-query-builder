<?php
namespace Test;

use PHPUnit\Framework\TestCase;

// putenv('PHPUNIT_RESULT_CACHE=/home/dimitarsi/.phpunit.cache');

final class QueryBuilderRelationsTest extends TestCase
{

/* #region  Settup */
    public function getMock()
    {
        $mockMethods = ["query", "escape"];
        $constArgs = [\Test\MockModel::class];

        $stub = $this->getMockBuilder(\QueryBuilder\QueryBuilder::class)
                    ->setConstructorArgs($constArgs)
                    ->setMethods($mockMethods)
                    ->getMock();

        $stub->method("escape")
             ->will($this->returnArgument(0));

        $stub->method("query")
             ->willReturn(new FakeDbResponse(['Id' => 1, 'name' => 'Foobar']));
        
        return $stub;
    }
/* #endregion */

/* #region  Select Tests */
    public function testQueryBuilderCanSelectAllFields() : void
    {
        $this->markTestSkipped('Implementation in progress');

        $table = MockModel::getTable();
        $expectedArgument = $this->stringContains("select * from $table");

        $queryBuilder = $this->getMock();
        $queryBuilder->expects($this->once())
                     ->method('query')
                     ->with($expectedArgument);



        $queryBuilder->with('relatedProducts');
    }
}


// class Category {

//     public function children() {

//     }

//     public function parents() {
        
//     }
// }

// class Product {

//     public function mainCategory() {
//         return $this->belongsTo(Category::class, 'parentId');
//     }

//     public function otherCategories() {
//         return $this->hasMany(Category::class, 'product_to_category', 'productId', 'categoryId');
//     }

//     public function relatedProducts() {

//     }
// }

// $product->relatedProductSettings();
// $resp = [
//     RelatedProduct(),
//     RelatedCategoryProducts(),
//     RelatedProductsByBrand()
// ];