<?php
namespace Test;

use PHPUnit\Framework\TestCase;
use QueryBuilder\Db;
use QueryBuilder\Raw;
use \QueryBuilder\QueryBuilder;

// putenv('PHPUNIT_RESULT_CACHE=/home/dimitarsi/.phpunit.cache');

final class QueryBuilderTest extends TestCase
{

/* #region  Settup */
    public function getMock()
    {
        $mockMethods = ["query", "escape"];
        $constArgs = [MockModel::class];

        $stub = $this->getMockBuilder(QueryBuilder::class)
                    ->setConstructorArgs($constArgs)
                    ->setMethods($mockMethods)
                    ->getMock();

        $stub->method("escape")
             ->will($this->returnArgument(0));

        $stub->method("query")
             ->willReturn(new FakeDbResponse(['Id' => 1, 'name' => 'Foobar']));
        
        return $stub;
    }
/* #endregion */

/* #region  Select Tests */
    public function testQueryBuilderCanSelectAllFields() : void
    {
        $expectedArgument = $this->stringContains("select * from ".MockModel::$tableName);

        $queryBuilder = $this->getMock();
        $queryBuilder->expects($this->once())
                     ->method('query')
                     ->with($expectedArgument);

        $queryBuilder->get();
    }

    public function testQueryBuilderCanSelectOnlySpecificFields() : void
    {
        $table = strtolower(MockModel::getTable());
        $fields = implode(', ', ['field1', 'field2']);
        $expectedArgument = $this->stringContains("select ".$fields." from $table");

        $queryBuilder = $this->getMock();
        $queryBuilder->expects($this->once())
                    ->method('query')
                    ->with($expectedArgument);

        $queryBuilder->get($fields);
    }

    public function testQueryBuilderCanSelectOnlyTheFirst() : void
    {
        $fields = implode(', ', ['field1', 'field2']);
        $table = strtolower(MockModel::getTable());
        $expectedArgument = $this->stringContains("select ".$fields." from $table LIMIT 1");

        $queryBuilder = $this->getMock();
        $queryBuilder->expects($this->once())
                    ->method('query')
                    ->with($expectedArgument);

        $queryBuilder->first($fields);
    }

    public function testQueryBuilderCanSelectWithOrder() : void
    {
        $fields = implode(', ', ['field1', 'field2']);
        $table = strtolower(MockModel::getTable());
        $expectedArgument = $this->stringContains("select ".$fields." from $table ORDER BY field1 ASC");

        $queryBuilder = $this->getMock();
        $queryBuilder->expects($this->once())
                    ->method('query')
                    ->with($expectedArgument);

        $queryBuilder->orderBy('field1')
                    ->get($fields);
    }

    public function testQueryBuilderCanSelectWithOrderAndLimit() : void
    {
        $fields = implode(', ', ['field1', 'field2']);
        $table = strtolower(MockModel::getTable());
        $expectedArgument = $this->stringContains("select ".$fields." from $table ORDER BY field1 ASC LIMIT 0,100");

        $queryBuilder = $this->getMock();
        $queryBuilder->expects($this->once())
                    ->method('query')
                    ->with($expectedArgument);

        $queryBuilder->orderBy('field1')
                    ->limit('0,100')
                    ->get($fields);
    }

/* #endregion */

/* #region  Insert Tests */
    public function testQueryBuilderCanInsertStringValues() : void 
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("insert into $table ( `field1`, `field2` ) values ( 'value1', 'value2' )");
        $mock = $this->getMock();

        $mock->expects($this->exactly(2))
            ->method("escape");

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);

        $mock->create([
            "field1" => "value1",
            "field2" => "value2"
        ]);
    }

    public function testQueryBuilderCanInsertMixedValues() : void 
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("insert into $table ( `field1`, `field2` ) values ( 'value1', 1 )");
        $mock = $this->getMock();

        $mock->expects($this->exactly(2))
            ->method("escape");

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);

        $mock->create([
            "field1" => "value1",
            "field2" => 1
        ]);
    }


    public function testQueryBuilderCanInsertRawValues() : void 
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("insert into $table ( `field1`, `field2`, `field3` ) values ( 'value1', 1, NOW() )");
        $mock = $this->getMock();

        $mock->expects($this->exactly(2))
            ->method("escape");

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);

        $mock->create([
            "field1" => "value1",
            "field2" => 1,
            "field3" => Db::Now()
        ]);
    }

    public function testQueryBuilderCanInsertCallableValues() : void 
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("insert into $table ( `field1`, `field2`, `field3`, `field4` ) values ( 'value1', 1, NOW(), 'From anonymous function' )");
        $mock = $this->getMock();

        $mock->expects($this->exactly(3))
             ->method("escape");

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);

        $mock->create([
            "field1" => "value1",
            "field2" => 1,
            "field3" => Db::Now(),
            "field4" => function() {
                return "From anonymous function";
            }
        ]);
    }
/* #endregion */

/* #region  Update Tests */
    public function testQueryBuilderCanUpdate() : void
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("update $table set `field1` = 'value1', `field2` = 1, `field3` = NOW(), `field4` = 'From anonymous function'");
        $mock = $this->getMock();

        $mock->expects($this->exactly(3))
             ->method("escape");

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);

        $mock->update([
            "field1" => "value1",
            "field2" => 1,
            "field3" => Db::Now(),
            "field4" => function() {
                return "From anonymous function";
            }
        ]);
    }
/* #endregion */

/* #region Where Tests */
    public function testQueryBuilderSimpleOrWhere() : void
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("select * from $table where ( `field1` = 1 or `field2` = 0 )");
        $mock = $this->getMock();

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);
        
        $mock->where('field1', 1)
            ->orWhere('field2', 0)
            ->get();
    }

    public function testQueryBuilderMultipleOrWhere() : void
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("select * from $table where ( `field1` = 0 or `field2` = 1 or `field3` = 2 ) and ( `active` = 1 or `include` = 0 )");
        $mock = $this->getMock();

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);
        
        $mock->where('field1', 0)
            ->orWhere('field2', 1)
            ->orWhere('field3', 2)
            ->where('active', 1)
            ->orWhere('include', 0)
            ->get();
    }

    public function testQueryBuilderMultipleOrWhereWithExtraColumns() : void
    {
        $table = strtolower(MockModel::getTable());
        $expectedArg = $this->stringContains("select * from $table where ( `field1` = 0 or `field2` = 1 or `field3` = 2 ) and ( `active` = 'should_be_active' or `include` = another_column ) ");
        $mock = $this->getMock();

        $mock->expects($this->once())
            ->method("query")
            ->with($expectedArg);
        
        $mock->where('field1', 0)
            ->orWhere('field2', 1)
            ->orWhere('field3', 2)
            ->where('active', 'should_be_active')
            ->orWhere('include', new Raw("another_column"))
            ->get();
    }
/* #endregion */
}
